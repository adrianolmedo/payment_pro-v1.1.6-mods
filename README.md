# payment_pro-v1.1.6-mods
vQmod script for [Promotional Packages System](https://github.com/codexilab/osclass-packages)'s integrations to [Payment Pro v1.1.6](https://bitbucket.org/adrianolmedo/osclass-payments-pro/src/openssl/)

## Instructions to integrate

1. Get payment_pro.xml
2. Create a Zip file of payment_pro.xml ([payment_pro-v1.1.6-mods.zip](https://bitbucket.org/adrianolmedo/payment_pro-v1.1.6-mods/downloads/payment_pro-v1.1.6-mods.zip))
3. Upload on your vQmod for Osclass
4. Enable.